#### 一、效果
在浏览器中显示 Hello React

#### 二、步骤

##### 2.1、需要用到的 js 脚本文件
- 1、react.js: React 的核心库。
- 2、react-dom.js: 提供操作 DOM  的react 扩展库。
- 3、babel.min.js: 解析 JSX 语法代码转为纯 JS 语法代码的库。

##### 2.2、新建 index.html 文件,页面中导入 js 脚本文件
```js
<script src="JS/react.development.js"></script><!--1、React 的核心库-->
<script src="JS/react-dom.development.js"></script><!--2、提供与 DOM 相关的功能-->
<script src="JS/babel.min.js"></script><!--3、内嵌了对 JSX 语法的支持（js脚本页面可混合写入 html 
```

##### 2.3、编码
```js
/*如果我们需要使用 JSX，则 <script> 标签的 type 属性需要设置为 text/babel*/
<script type="text/babel">

ReactDOM.render(
  <h1>Hello, world!</h1>,
  document.getElementById('example')
);

</script>
```
可使用 React DeveloperTools 工具观察DOM 元素
##### 2.4、完整代码
```html
<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8" />
  <title>Hello React!</title>

  <script src="JS/react.development.js"></script><!--1、React 的核心库-->
  <script src="JS/react-dom.development.js"></script><!--2、提供与 DOM 相关的功能-->
  <script src="JS/babel.min.js"></script><!--3、内嵌了对 JSX 语法的支持（js脚本页面可混合写入 html 代码）-->

  <script type="text/babel">/*如果我们需要使用 JSX，则 <script> 标签的 type 属性需要设置为 text/babel*/

  ReactDOM.render(
      <h1>Hello, world!</h1>,
      document.getElementById('example')
  );

  </script>

</head>
<body>

<div id="example"></div>



</body>
</html>
```